package com.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class CornerCasesTest {
    private SortingApp sortingApp = new SortingApp();

    private int[] arrayTest;
    private String expected;

    public CornerCasesTest(int[] arrayTest, String expected) {
        this.arrayTest = arrayTest;
        this.expected = expected;
    }


    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgsCase(){
        int[] zeroArgsArray = new int[0];
        sortingApp.sort(zeroArgsArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgCase(){
        int[] oneArgArray = new int[1];
        sortingApp.sort(oneArgArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgsCase(){
        int[] tenPlusArgsArray = new int[11];
        sortingApp.sort(tenPlusArgsArray);
    }

    @Test
    public void testTwoToTenAgrsCase(){
        assertEquals(expected, sortingApp.sort(arrayTest));
    }

    @Parameterized.Parameters
    public static Collection input(){
        int[]   arraytest1 = {6, 5},
                arraytest2 = {11, 10, -2, 0},
                arraytest3 = {2, 4, 1, 6, 5, 12},
                arraytest4 = {10, 9, 8, 6, 7, 5, 4, 1, 3, 2};
        return Arrays.asList(new Object[][]{
                {arraytest1, "[5, 6]"},
                {arraytest2, "[-2, 0, 10, 11]"},
                {arraytest3, "[1, 2, 4, 5, 6, 12]"},
                {arraytest4, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}});
    }
}
