package com.epam;

import java.util.Arrays;
import java.util.Scanner;

import static java.util.Arrays.sort;

/**
 * Hello world!
 *
 */
public class SortingApp {
    public static void main( String[] args ) {
        System.out.println("Enter up to 10 separated by space bar");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String[] strings = input.split(" ");

        int[] arguments = new int[strings.length];

        for (int i = 0; i < strings.length; i++) {
            arguments[i] = Integer.parseInt(strings[i]);
        }
        sort(arguments);
        System.out.println(Arrays.toString(arguments));
    }

    public static String sort(int[] array) {
        if (array.length == 0) {
            throw new IllegalArgumentException("Zero arguments passed");
        }
        if (array.length == 1) {
            throw new IllegalArgumentException("One argument passed");
        }
        if (array.length > 10) {
            throw new IllegalArgumentException("More than ten arguments");
        }

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j <array.length; j++) {
                if (array[j] < array[i]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return Arrays.toString(array);
    }
}
